#!/bin/bash

mkdir -p www/flaskapp
cd www/flaskapp
source flaskenv/bin/activate
gunicorn --bind 0.0.0.0:5000 app:app &